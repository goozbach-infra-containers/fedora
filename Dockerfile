FROM docker.io/library/fedora:latest

RUN dnf -y install git tree vim-enhanced stow ; yum clean all

RUN echo "%wheel        ALL=(ALL)       NOPASSWD: ALL" > /etc/sudoers.d/wheel

RUN mkdir -p /opt/src

RUN useradd -u 50020 -G wheel goozbach

WORKDIR /opt/src

